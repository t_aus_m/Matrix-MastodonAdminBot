# Matrix-MastodonReportBot

This bot is able to send notifications for new pending reports and accounts on a given Mastodon-instance to one or multiple matrix-rooms.

It uses Node.js and can be easily deployed using docker.

## Configuration

To use the bot, there need to be two Files `config.json` and `secrets.json`, which need to be located under the `/config`-subdirectory.

### `config.json`

- `apiPollIntervall`: Sets the time intervall, at which the bot checks the mastodon instance for new pending accounts/reports
- `setupMode`: The bot will run in setup mode and join every room, it is invited to

### `secrets.json`

- `homeserverURL`: The URL of the matrix-instance to be used
- `matrixSecret`: Secret of the matrix-account to be used, can be most easily optained using Riot ("Settings" → "Help & About" → "Access Token")
- `mastodonURL`: Root-URL of the Mastodon-instance to be used
- `mastodonSecret`: Access token for the given Mastodon-instance, needs to have the `admin:read:accounts` and the `admin:read:reports` rights. Can be optained in the WebUI ("Settings" → "Development" → "New Application", and after creating the access token: "Your access token" )