const util = require('util');
const sdk = require("matrix-bot-sdk");
const MatrixClient = sdk.MatrixClient;
const SimpleFsStorageProvider = sdk.SimpleFsStorageProvider;
const AutojoinRoomsMixin = sdk.AutojoinRoomsMixin;
const AutojoinUpgradedRoomsMixin = sdk.AutojoinUpgradedRoomsMixin;
const fs = require('fs');
const request = require('request');
const Mastodon = require('mastodon-api');
const response = require('express');
const path = require('path');
const setIntervalPromise = util.promisify(setInterval);

const _secrets = fs.readFileSync(path.resolve('./config','secrets.json'));
const secrets = JSON.parse(_secrets);

const _conf = fs.readFileSync(path.resolve('./config','config.json'));
const conf = JSON.parse(_conf);


const storage = new SimpleFsStorageProvider("bot.json");
const client = new MatrixClient(secrets.homeserverUrl, secrets.matrixSecret, storage);

AutojoinUpgradedRoomsMixin.setupOnClient(client);

const masto = new Mastodon({
	"access_token": secrets.mastodonSecret,
	"timeout_ms": 60 * 1000,
	"api_url": `${secrets.mastodonUrl}/api/v1/`
});

const apiWatchdogInterval = conf.apiPollIntervall;

if (conf.setupMode) {
	AutojoinRoomsMixin.setupOnClient(client)
};

client.start().then(() => console.log("Client started!"));
apiWatchdog();

if (!fs.existsSync('data.json')) {
	console.log("Data storage initializted");
	var data = '{ "report" : null, "accounts" : [], "hashtags": [], "rooms" : []}';
	data = JSON.stringify(JSON.parse(data), null, "\t");
	fs.writeFileSync('data.json', data)
};

client.on("room.message", (roomId, event) => {
	if (!event["content"]) return;
	var body = event["content"]["body"];

	if (body.startsWith("!listreports")) {
		masto.get('admin/reports', {}).then(response => {
			if (response.data.length == 0) {
				client.sendMessage(roomId, {
					"msgtype": "m.text",
					"body": "There are no open reports currently!"
				})
			} else {
				var out_form = `These are all reports that are currently open:\n<ul>`;
				var out_unform = `These are all reports that are currently open:\n`;
				while (response.data.length > 0) {
					var curr = response.data.pop();
					out_form += `<li>
						<a href=\"${curr.target_account.account.url}\">${curr.target_account.account.acct}</a>
						<ul>
						<li><b>Date</b>: ${curr.created_at}</li>
						<li><b>By</b>: <a href=\"${curr.account.account.url}\">`;
					out_unform += `- ${curr.target_account.account.acct} (${curr.target_account.account.url})\n
						\t- Date: ${curr.created_at}\n\t- By: @`;
					if (curr.account.domain === null) {
						out_form += curr.account.username;
						out_unform += curr.account.username
					} else {
						out_form += `${curr.account.username}@${curr.account.domain}`;
						out_unform += `${curr.account.username}@${curr.account.domain}`
					}
					out_form += `</a></li>
						<li><b>Reason</b>: "${curr.comment}"</li>
						<li><b>URL</b>: <a href=\"${secrets.mastodonUrl}/admin/reports/${curr.id}\">
							${secrets.mastodonUrl}/admin/reports/${curr.id}</a></li></ul></li>`;
					out_unform += ` (${curr.account.account.url})\n
						\t- Reason: "${curr.comment}"\n
						\t- URL: ${secrets.mastodonUrl}/admin/reports/${curr.id}`
				}
				out_form += `</ul>`
			};
			client.sendMessage(roomId, {
				"msgtype": "m.text",
				"format": "org.matrix.custom.html",
				"body": out_unform,
				"formatted_body": out_form
			})
		})
	} else if (body.startsWith("!listhashtags")) {
		// TODO: do stuff
		if (response.data.length == 0) {
			client.sendMessage(roomId, {
				"msgtype": "m.text",
				"body": "Mastodon does not currently support moderation of Hahtags via API. As soon as it does, this Feature will be implemented."
			})
		}
	} else if (body.startsWith("!listaccounts")) {
		masto.get('admin/accounts', { 'pending': 'true' }).then(response => {
			if (response.data.length == 0) {
				client.sendMessage(roomId, {
					"msgtype": "m.text",
					"body": "There are no accounts pending for review!"
				})
			} else {
				var out_form = `These are all accounts currently pending for review:\n<ul>`;
				var out_unform = `These are all accounts currently pending for review:\n`;
				while (response.data.length > 0) {
					var curr = response.data.pop();
					out_form += `<li>@${curr.username}<ul>
						<li><b>E-Mail</b>: ${curr.email}</li>
						<li><b>Created</b>: ${curr.created_at}</li>
						<li><b>Request</b>: ${curr.invite_request}</li>
						<li><b>URL</b>: <a href=\"${secrets.mastodonUrl}/admin/accounts/${curr.id}\">
							${secrets.mastodonUrl}/admin/accounts/${curr.id}</a></li></ul></li>`;
					out_unform += `- @${curr.username}\n
						\t- E-Mail: ${curr.email}\n
						\t- Created: ${curr.created_at}\n
						\t- Request: ${curr.invite_request}\n
						\t- URL: ${secrets.mastodonUrl}/admin/accounts/${curr.id}\n`
				};
				out_form += `</ul>`
			};
			client.sendMessage(roomId, {
				"msgtype": "m.text",
				"format": "org.matrix.custom.html",
				"body": out_unform,
				"formatted_body": out_form
			})
		})
	} else if (body.startsWith("!notify")) {
		var _body = body.split(" ");
		console.log(_body);
		var rooms = fs.readFileSync('data.json');
		rooms = JSON.parse(rooms);
		console.log(rooms);
		if (_body[1] == "on") {
			if (rooms.rooms.includes(roomId)) {
				client.sendMessage(roomId, {
					"msgtype": "m.text",
					"body": "Automatic notifications is already enabled for this room!"
				})
			} else {
				client.sendMessage(roomId, {
					"msgtype": "m.text",
					"body": "Automatic notifications for new pending accounts, reports and hashtags has been enabled for this room!"
				});
				rooms.rooms.push(roomId)
			}
		} else if (_body[1] == "off") {
			if (rooms.rooms.includes(roomId)) {
				client.sendMessage(roomId, {
					"msgtype": "m.text",
					"body": "Automatic notifications for new pending accounts, reports and hashtags has been disabled for this room!"
				});
				rooms.rooms = rooms.rooms.filter(element => element != roomId)
			} else {
				client.sendMessage(roomId, {
					"msgtype": "m.text",
					"body": "Automatic notifications is already disabled for this room!"
				})
			}
		} else {
			client.sendMessage(roomId, {
				"msgtype": "m.text",
				"format": "org.matrix.custom.html",
				"body": `Unknown command. Use !notify(on | off) to turn on/ off automatic notifications about new pending reports, hashtags and accounts`,
				"formatted_body": `Unknown command. Use <b>!notify (on|off)</b> to turn on/off automatic notifications about new pending reports, hashtags and accounts\n`
			})
		};
		rooms = JSON.stringify(rooms, null, "\t");
		fs.writeFileSync('data.json', rooms)
	} else if (body.startsWith("!help")) {
		client.sendMessage(roomId, {
			"msgtype": "m.text",
			"format": "org.matrix.custom.html",
			"body": `This is the help page for the Mastodon admin bot.\n
				Available Commands are: \n
				- !help to display this page\n
				- !listreports to display all open reports\n
				- !listhashtags to display all hashtags awaiting confirmation (Not yet implemented :( )\n
				- !listaccounts to display all accounts awaiting confirmation\n
				- !notify (on|off) to turn on/off automatic notifications about new pending reports, hashtags and accounts`,
			"formatted_body": `This is the help page for the Mastodon admin bot.\n
				Available Commands are:\n<ul>
				<li><b>!help</b> to display this page</li>
				<li><b>!listreports</b> to display all open reports</li>
				<li><b>!listhashtags</b> to display all hashtags awaiting confirmation. <i>Not yet implemented :( </i></li>
				<li><b>!listaccounts</b> to display all accounts awaiting confirmation</li>
				<li><b>!notify (on|off)</b> to turn on/off automatic notifications about new pending reports, hashtags and accounts</li></ul>\n`
		})
	}
});

async function apiWatchdog() {
	setIntervalPromise(async function () {
		var lastData = fs.readFileSync('data.json');
		lastData = JSON.parse(lastData);
		var lastReport = lastData.report;
		var lastAccounts = lastData.accounts;
		var lastHasshtags = lastData.hashtags;
		var rooms = lastData.rooms;
		var reports = await masto.get('admin/reports', {}).then(response => { return response.data });
		var accounts = await masto.get('admin/accounts', { 'pending': 'true' }).then(response => { return response.data });
		var newReport = null;
		var newAccounts = new Array;

		reports.forEach(element => {
			if (element.id > lastReport) {
				var out_form = `There is an new pending report:\n
					<li><a href=\"${element.target_account.account.url}\">
						${element.target_account.account.acct}</a><ul>
					<li><b>By</b>: <a href=\"${element.account.account.url}\">`;
				var out_unform = `There is a new pending report:\n
					- ${element.target_account.account.acct} (${element.target_account.account.url})\n\t- By: @`;
				if (element.account.domain === null) {
					out_form += element.account.username;
					out_unform += element.account.username
				} else {
					out_form += `${element.account.username}@${element.account.domain}`;
					out_unform += `${element.account.username}@${element.account.domain}`
				};
				out_form += `</a></li>
					<li><b>Reason</b>: "${element.comment}"</li>
					<li><b>URL</b>: <a href=\"${secrets.mastodonUrl}/admin/reports/${element.id}\">
						${secrets.mastodonUrl}/admin/reports/${element.id}</a></li></ul></li>`;
				out_unform += ` (${element.account.account.url})\n
					\t- Reason: "${element.comment}"\n
					\t- URL: ${secrets.mastodonUrl}/admin/reports/${element.id}`;
				rooms.forEach(room => {
					client.sendMessage(room, {
						"msgtype": "m.text",
						"format": "org.matrix.custom.html",
						"body": out_unform,
						"formatted_body": out_form
					})
				});
				newReport = element.id
			} else {
				newReport = lastReport
			}
		});
		accounts.forEach(element => {
			if (!lastAccounts.includes(element.id)) {
				var out_form = `There is a new account pending for review:\n
					<ul><li>@${element.username}<ul>
					<li><b>E-Mail</b>: ${element.email}</li>
					<li><b>Created</b>: ${element.created_at}</li>
					<li><b>Request</b>: ${element.invite_request}</li>
					<li><b>URL</b>: <a href=\"${secrets.mastodonUrl}/admin/accounts/${element.id}\">
						${secrets.mastodonUrl}/admin/accounts/${element.id}</a></li></ul></li></ul>`;
				var out_unform = `There is a new account pending for review\n
					- @${element.username}\n
					\t- E-Mail: ${element.email}\n
					\t- Created: ${element.created_at}\n
					\t- Request: ${element.invite_request}\n
					\t- URL: ${secrets.mastodonUrl}/admin/accounts/${element.id}\n`;
				rooms.forEach(room => {
					client.sendMessage(roomId, {
						"msgtype": "m.text",
						"format": "org.matrix.custom.html",
						"body": out_unform,
						"formatted_body": out_form
					})
				})
			};
			newAccounts.push(element.id)
		});

		var newData = JSON.parse(JSON.stringify(lastData));
		newData.report = newReport;
		newData.accounts = newAccounts;
		newData = JSON.stringify(newData, null, "\t");
		fs.writeFileSync('data.json', newData)
	}, apiWatchdogInterval);
}